# Symfony project template

**Table of contents**

* [Structure overview](#overview)
* [Requirements](#requirements)
* [Setup](#setup)
  * [Setup Symfony](#setup-symfony)
  * [Setup Composer](#setup-composer)
  * [Cleanup Symfony installation](#cleanup-symfony)
* [Configuration](#configuration)
    * [Symfony configuration](#symfony-configuration)
    * [Kubernetes configuration](#kubernetes-configuration)
* [Send web requests to your app](#sending-requests)
* **[DEV-Environment](#dev-env)**
    * [Build the container](#build-container-dev)
    * [Run container](#run-container-dev)
    * [Connect to container](#connect-container-dev)
* **[PROD-Environment](#prod-env)**



## Structure overview <a id="overview" href="#overiew">#</a>

| directory | description |
| --------- | ----------- |
| ./        | Project root |
| ./app/    | Symfony app root |
| ./docker/ | Dockerfiles and image configuration files |
| ./k8s/    | Kubernetes configuration files |

## Requirements <a id="requirements" href="#requirements">#</a>
* Docker 19.03.0+
* Docker Compose 1.27.0+

## Setup <a id="setup" href="#setup">#</a>

### Setup Symfony <a id="setup-symfony" href="#setup-symfony">#</a>
First you have to decide, wether you want to create a microservice or a webapp.

In case of a webapp, use this command:
```shell
symfony new app --webapp
```

In case of a microservice, use this command:
```shell
symfony new app
```

If you require to use a specific Symfony version, add the version parameter to the commands above. For example, if you want to create a Symfony 5.4 webapp, use this command:
```shell
symfony new app --webapp --version=5.4
```
It might be a good idea to use the version "lts" instead of a version number.

### Setup Composer <a id="setup-composer" href="#setup-composer">#</a>
After Symfony was installed, create a new file inside the app folder, called **compose.env**<br/>
Later, this file will be used for setting special environment values for docker, if required.

### Cleanup Symfony installation <a id="cleanup-symfony" href="#cleanup-symfony">#</a>
Symfony will always be created with .git folder and a .gitignore file, inside the app folder.
You need to remove the .git folder, to be able to add the app to your repository. You should
also remove the .gitignore file, to prevent unexpected behavior.

If Symfony 6+ was used for the new app, Symfony might have created a **docker-compose.yml**
and a **docker-compose.overrides.yml** inside the app folder. You should remove both files.

## Configuration <a id="configuration" href="#configuration">#</a>

### Symfony configuration <a id="configuration" href="#configuration">#</a>
Symfony comes with ENV-file support. After the new application is created, you can find a .env file inside the app directory.

You should always setup configurations, that have to change between dev and prod, in this file.

To use these environment variables, you can parse them, in the symfony configuration YAML files, like
```yaml
MYKEY: "%env(resolve:MY_ENV_VAR)%"
```

[Read more about Symfony configuration here](https://symfony.com/doc/current/configuration.html)

#### Docker Compose extra configuration

If you need to use special configuration values in your local docker dev environment, you can use the **compose.env** file inside the app/ folder.
With this file, you are able to override the configuration values from the .env file.

This is, for example, helpful if you want to include a test database inside your docker-compose.yml.

### Kubernetes configuration <a id="configuration" href="#configuration">#</a>
Kubernetes configuration files for DEV and PROD environments are already created, in k8s folder. 
These files are created to work in most cases.

You just need to change the service name. To simplify this process, you can find-replace the keyword "SERVICENAME" with the real service name.<br/>
Also take a look at the lines 30 and 71, in both files.
* At line 30, the docker hub image name is configured. If this is not equal to "docker.pfwiaas.com/SERVICENAME:latest", change this line.
* At line 71, the host name is configured. If this is not equal to "SERVICENAME.pfwiaas.com", change this line.

## Send web requests to your app <a id="sending-requests" href="#sending-requests">#</a>
By default, your local dev environment is reachable at: [localhost:8080](http://localhost:8080).

If you want to run multiple docker apps in parallel, you should either change the ports in your docker-compose.yml files or use minikube, to handle the connection management.

## DEV-Environment <a id="dev-env" href="#dev-env">#</a>
This part is meant for local development. These steps will not work for K8s.

Using these steps will build the project with a connected local database
container, based on mariaDB. Also the app folder will be loaded as volume, so
every change you make, will be directly in sync with the container.

All commands are to execute inside the project root directory.

### Build the container  <a id="build-container-dev" href="#build-container-dev">#</a>
Before building the container for the first time, open the docker-compose.yml file and replace the string CONTAINER_NAME with the name of your application

If you just checked out the project or made changes at the files related to
docker, you have to build it with this command:
```shell
docker-compose up --build
```
For debug mode, you can use the ```-d``` option.

### Run container  <a id="run-container-dev" href="#run-container-dev">#</a>
Once you have build the container, you can run it everytime with this command:
```shell
docker-compose up
```
For debug mode, you can use the ```-d``` option.

### Connect to container  <a id="connect-container-dev" href="#connect-container-dev">#</a>
To connect to your running container, you can use this command:
```shell
docker-compose exec CONTAINER_NAME /bin/bash
```

## PROD-Environment <a id="prod-env" href="#prod-env">#</a>
TODO: add Dteails for production deployment or remove this part.
