FROM php:8.1.2-fpm-alpine3.15
# update system & install applications && install libraries
RUN apk update && apk upgrade && \
    apk add bash nginx mariadb mariadb-client git zip curl supervisor openssh busybox-extras openrc --no-cache
ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/
RUN chmod +x /usr/local/bin/install-php-extensions
RUN install-php-extensions intl apcu mysqli pdo_mysql memcache memcached opcache
# Configure nginx
COPY docker/nginx/nginx_default.conf /etc/nginx/conf.d/default.conf
COPY docker/nginx/nginx.conf /etc/nginx/nginx.conf
# Configure php-fpm
RUN chmod 777 /run
COPY docker/php/php-fpm.conf /usr/local/etc/php-fpm.conf
COPY docker/php/fpm-pool.conf /usr/local/etc/php-fpm.d/www.conf
# Configure supervisord
COPY ./docker/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
# Copy env and run script
COPY ./docker/run.sh /run.sh
# setup composer and symfony
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer;
RUN curl -sS https://get.symfony.com/cli/installer | bash && mv /root/.symfony/bin/symfony /usr/local/bin/symfony
# create nginx user
RUN adduser -D -g 'www' www
# make sure credentials are working
RUN mkdir -p /var/www/app
RUN chown -R www:www /var/www && \
    chown -R www:www /run && \
    chown -R www:www /var/lib/nginx && \
    chown -R www:www /var/log && \
    chown -R www:www /usr/local/bin && \
    chown -R www:www /run.sh && \
    chmod +x /run.sh
# set work parameters
USER www
COPY --chown=www ./app /var/www/app
WORKDIR /var/www/app
# expose port
EXPOSE 80
# start system
RUN /bin/sh /run.sh
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]
