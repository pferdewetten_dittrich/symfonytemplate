#!/bin/sh

if [[ ! -f /home/www/.gitconfig ]]; then
  touch /home/www/.gitconfig
fi
git config --global user.name "Pferdewetten AG" && \
    git config --global user.email "git@pferdewetten.de"

if [[ ! -f /home/www/.bashrc ]]; then
  touch /home/www/.bashrc
  echo '#!/bin/bash' > /home/www/.bashrc
fi
echo 'alias ll="ls -lAhF --color=auto"' >> /home/www/.bashrc
echo 'alias serve-log="tail -f /var/log/nginx/*.log /var/log/php7/*.log"' >> /home/www/.bashrc

symfony check:requirements

if [[ -f /var/www/app/composer.json ]]; then
  composer validate --no-check-publish && composer recipes ; else echo 'No package.json found' ;
fi
if [[ ! -f /var/www/app/symfony.lock ]]; then
  echo "Symfony project not found"
fi
